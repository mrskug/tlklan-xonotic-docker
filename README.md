## Xonotic servers for TLK LAN

### Quickstart:
1. Clone repo
2. Edit `base.env.example` and copy to `base.env`
3. Run `make firstrun` (wait for download + extract to finish then -> Ctrl+C)
4. (Optional) set rcon password in (banan|tomat|chili)/env
5. Run `make`
