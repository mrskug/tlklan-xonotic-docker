## Makefile for Xonotic configs

define setup_env
	$(eval ENV_FILE := ./$(1)/env)
	@echo " - setup env $(ENV_FILE)"
	$(eval include ./$(1)/env)
	$(eval export sed 's/=.*//' ./$(1)/env)
endef

default: banan tomat chili
	@if [ -d xonotic ]; then docker-compose up -d && docker-compose logs -f; else echo "game folder is missing, please use 'make firstrun' to download the files"; fi

firstrun:
	@echo "This will start up one container to download Xonotic, once the download is finished exit the container and start all containers with 'make defalt'"
	read -e -p "Continue? " choice ; [[ "$$choice" == [Yy]* ]] && echo "Proceeding.." || exit 1
	@docker-compose up banan

start:	
	@docker-compose start

restart:	
	@docker-compose restart

stop:
	@docker-compose stop

clean:  stop
	@docker-compose rm

banan:
	$(call setup_env,$@)
	@cp $@/config xonotic/data/$@.cfg
	@sed -i 's/"CHANGE_RCON_PASSWORD"/"${RCON_PASSWORD}"/g' xonotic/data/$@.cfg

tomat:
	$(call setup_env,$@)
	@cp $@/config xonotic/data/$@.cfg
	@sed -i 's/"CHANGE_RCON_PASSWORD"/"${RCON_PASSWORD}"/g' xonotic/data/$@.cfg

chili:
	$(call setup_env,$@)
	@cp $@/config xonotic/data/$@.cfg
	@sed -i 's/"CHANGE_RCON_PASSWORD"/"${RCON_PASSWORD}"/g' xonotic/data/$@.cfg

test:
	if [ -d xonotic ]; then echo "derp"; else echo "herp"; fi
